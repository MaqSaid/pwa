FROM alpine:3.7

EXPOSE 8000

RUN apk update && \
  apk add nodejs bash yarn

RUN adduser -D pwa
USER pwa
WORKDIR /home/pwa

# install npm modules
COPY ./package.json /home/pwa
COPY ./yarn.lock /home/pwa
RUN export NODE_ENV=production && \
  yarn install --ignore-scripts

# copy into the container the pre-built production build
COPY ./dist /home/pwa/dist

CMD export NODE_ENV=production && yarn run start
