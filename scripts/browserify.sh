#! /bin/bash

if [ "$1" == "-w" ]; then
    cmd=node_modules/watchify/bin/cmd.js
else
    cmd=node_modules/browserify/bin/cmd.js
fi

MODULES_DIR=dist/public/js/modules
BROWSERIFY_DIR=dist/public/js/browserify

if [ -e "${BROWSERIFY_DIR}" ]; then
    rm ${BROWSERIFY_DIR:?}/*
else
    mkdir "${BROWSERIFY_DIR}"
fi

# Presumes no spaces in file name!
jsIn=`ls ${MODULES_DIR}/*.js`
browserifyIn=(${jsIn})

browserifyOut=()
for file in "${browserifyIn[@]}"; do
    file_="${BROWSERIFY_DIR}/${file##*/}"
    echo "${file} --> ${file_}"
    browserifyOut=("${browserifyOut[@]}" "-o ${file_}")
done

$cmd "${browserifyIn[@]}" \
     -t [ browserify-css ] -p \
    [ 'factor-bundle' ${browserifyOut[@]} ] \
    -x jquery -t [ browserify-css --global babelify --presets [ env ] ]] -o "./${BROWSERIFY_DIR}/bundle.js"
