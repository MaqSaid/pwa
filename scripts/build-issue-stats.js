const shell = require('shelljs');
const fs = require('fs');
const fetch = require('node-fetch');

if (process.env.NODE_ENV !== 'production') {
  console.log('skipping generation of issue stats');
  return;
}

var API_TOKEN = 'CYeCYUj1w9bKJjW4kyZz';
var OUT_PATH = 'dist/views/contributions.html';
var projects = ['5032677', '5334032', '5344842'];

function generateHtml(issues, users) {
  const keys = Object.keys(issues);
  keys.sort((key1, key2) => {
    return issues[key1] < issues[key2];
  });
  try {
    var file = fs.openSync(OUT_PATH, 'w');
    fs.writeSync(file, '<ul class="contribution-list">');
    keys.forEach(username => {
      user = users[username];
      fs.writeSync(
        file,
        `<li><a target="_blank" href="${user.web_url}"><img src="${
          user.avatar_url
        }"></img></a>
        <ul><li>@${username}</li><li>${issues[username]} issues</li></ul></li>`
      );
    });
    fs.writeSync(file, '</ul>');
  } finally {
    if (file) {
      fs.closeSync(file);
    }
  }
}

function updateIssues(issuesMap, userMap, issuesJson) {
  issuesJson.forEach(issue => {
    var user = issue.assignee;
    if (!user) {
      return;
    }
    var count = issuesMap[user.username];
    if (!count) {
      issuesMap[user.username] = 1;
      userMap[user.username] = user;
      return;
    }
    issuesMap[user.username] = ++count;
  });
}

function getClosedIssuesAsync(project, issuesMap, usersMap, page = 1) {
  let totalPages = 1;
  return fetch(
    `https://gitlab.com/api/v4/projects/${project}/issues?scope=all&state=closed&page=${page}`,
    {
      headers: { 'PRIVATE-TOKEN': `${API_TOKEN}` }
    }
  )
    .then(resp => {
      totalPages = parseInt(resp.headers.get('x-total-pages'));
      return resp.json();
    })
    .then(json => {
      updateIssues(issuesMap, usersMap, json);
      if (page < totalPages) {
        return getClosedIssuesAsync(project, issuesMap, usersMap, ++page);
      }
    });
}

function buildStatsAsync() {
  var issuesMap = {};
  var usersMap = {};
  var promises = [];
  projects.forEach(projectID => {
    console.log(`Getting issues for project ${projectID}...`);
    promises.push(getClosedIssuesAsync(projectID, issuesMap, usersMap));
  });
  Promise.all(promises)
    .then(() => {
      generateHtml(issuesMap, usersMap);
      console.log(issuesMap);
    })
    .catch(err => {
      console.log(err);
    });
}

buildStatsAsync();
