const shell = require('shelljs');
const fs = require('fs');

// $script, polyfill and Intercooler are in global namespace
const mainjsFiles = [
  'node_modules/scriptjs/dist/script.min.js',
  'node_modules/babel-polyfill/dist/polyfill.min.js',
  'vendor/zepto/dist/zepto.min.js',
  'node_modules/intercooler/dist/intercooler.js'
];

// create main.js
shell.exec(
  `node_modules/uglify-es/bin/uglifyjs --compress --mangle -- ${mainjsFiles.join(
    ' '
  )} > dist/public/js/main.js`
);
