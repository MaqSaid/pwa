#!/bin/bash

# start a json-sever instance in the background,
# but make sure it gets cleaned up when
# this script is terminated or exits.
# https://stackoverflow.com/questions/360201/how-do-i-kill-background-processes-jobs-when-my-shell-script-exits
trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT

# allow script to be run from any directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd "$DIR/../json-server" && yarn start &
cd "$DIR/../" && yarn test