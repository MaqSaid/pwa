module.exports = {
  globDirectory: 'dist/public/',
  globPatterns: ['**/*.{js,map,css,otf,eot,svg,ttf,woff,woff2,png}'],
  swDest: 'dist/public/sw.js',
  globIgnores: ['../workbox-cli-config.js']
};
