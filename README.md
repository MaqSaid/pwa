 OneRelief Donation App

The skeleton for this app was originally derived from this 
[starter template](https://github.com/Microsoft/TypeScript-Node-Starter).

[Staging URL](https://onerelief-staging-pwa.herokuapp.com/)

## Overview

The OneRelief PWA is a classic framework-less mutlipage web app, with Ajax where appropriate, delivered on Node.js.

It allows users to see what fund raising campaigns OneRelief is supporting and donate an amount of money should they desire.

The following key technologies are used:

* TypeScript
* [Braintree](https://www.braintreepayments.com) payments
* [pug](https://pugjs.org) templates
* [intercooler.js](http://intercoolerjs.org/)
* [SASS](http://http://sass-lang.com/)
* [Workbox](https://developers.google.com/web/tools/workbox/)
* [Browserify](http://browserify.org/)
* [Jest](https://facebook.github.io/jest/)
* [Puppeteer](https://github.com/GoogleChrome/puppeteer)
* [Prettier](https://prettier.io/)


All source code is written in TypeScript, apart from the very small amount of code embedded in pug templates for 
template logic. TypeScript was chosen primarily as a developer aid but also to try and reduce the presence of type 
related defects. A single functional coding style has been adopted. This avoids any temptation to try and turn 
JavaScript into Java and maintains a consistency to the code base.

Donations are made via the Braintree service. Credit Cards, PayPal and where appropriate on mobile Google Pay and Apple
Pay can be used.

Intercooler gives a lightweight declarative way to do simple Ajaxing of content, but is surprisingly powerful, client
side interaction can be achieved with relative ease using its approach. Additional client side JavaScript that needs to
be added is delivered in a modular fashion by way of Browserify. Browserify can also handle CSS imports within modules
of client side JavaScript, which allows contained pieces of the app to be downloaded (on demand if necessary).

A SASS CSS build system is used to compose a single CSS file. Picnic CSS was used as a starting point.

Workbox is used to help with organising the caching of files in the browser by the Service Worker.

Testing is a mixture of functional (Jest) and integration (Puppeteer) tests.

## Developer Guidelines

A linux shell is required to run the scripts and tooling.

### TypeScript

TypeScript is written in a module/function style. Use of the `class` keyword is forbidden. There are non-formatting
linting rules in `.tslint` and the project must pass the linting as part of the NPM `test` script. This is the
recommended linting presets with some small changes to aid consistency. 
It is not set in stone! As for formatting (i.e. visual) rules, `.ts` files must activate 
[Prettier](https://prettier.io/) on save.

Given that this is a TypeScript project, the best development experience will be with VSCode, although it is not
mandated. As long as your editor shows tslinting errors, and runs Prettier on save of `.ts` files then you should be
good to go!

`yarn watch` will transpile `.ts` files, bundle CSS, and Browserify TypeScript files for the browser.

The target browser is defined in `.babelrc`. This file is currently used by Browserify, which takes care of language
features. **TODO** lint code based on .babelrc for *browser* features used in the code base, including CSS.

### layout.pug

There shouldn't be any need to add script tags or CSS link tags to the head of the document.

### pug templates

Templates should be relatively small in length, making use of mixins where possible. Even if the mixin is not being
reused it aids readability.

It's possible to include pug templates directly (ie not using the mixin syntax) however this should be avoided as it
uses implicit scope of variables in the parent template which can be confusing.

Use of script tags within pug templates and JavaScript event handlers should be avoided. Client side JavaScript should
come from a separate Browserify-ed file, loaded by ScriptJS. There are plenty of examples in the codebase.

### CSS organisation
 
 TODO

### Google Puppeteer
 
 TODO

### GitLab

GIT branches should be **squashed** prior to merge request to avoid excessive GIT noise.

### yarn

yarn's task scripting functionality is used as the build system.

The key tasks are:

| Task | Description |
| :--- | :--- |
| build | Performs a full build of the project into the output `dist` directory |
| install | Downloads all project dependencies into `node_modules` |
| start | Starts the application running on a Node.js instance |
| test | Runs both functional then integration tests |
| test-functional | Runs only functional tests |
| test-integration | Runs only integration tests |
| tslint | run the typescript linter |
| watch | Starts an instance of Node.js and runs the application while watching for changes to source files, including SASS and Browserify input files |

### Environment variables

The app needs several environment variables set to function correctly, these can be found in the file `.env.example`

## Docker

### Build

Before building the Docker image using `local.Dockerfile`, the project should be built using `yarn build`. The `dist` folder is copied during the image build. During the image build the npm dependencies are downloaded, however due to the way layers work in Docker subsequent builds should be faster.

e.g. `docker build . -f local.Dockerfile -t pwa` to build an image labelled `pwa:latest`.

The main Docker file `Dockerfile` is for CI deployment. The main difference is that the source code is downloaded over the network. This is because the Docker daemon in the Gitlab runner configuration is external to the CI container image and doesn't have access to the CI container's file system. See [Docker socket binding](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html).

### Run

To run the Docker image locally:

* Supply a port mapping for Node.js
* Give the container access to the env variables
* Make sure Node.js can communicate with the API url

The `--net="host"` setting can be used to point `localhost` in the container at the host machine. For example, where `pwa` is the name of the Docker image, and `pwa` is the intended name of the container:

`docker run --net="host" --env-file .env -p 8000:8000 --name pwa pwa`

## Heroku Deploy

The env variables in `.env.example` should be manually added to the Heroku config variables on the Heroku app settings page. (`NODE_ENV=development` is currently required for Git based deploys)

The Mailgun Heroku add-on can be used if email sending is required. The add-on will add a number of config variables to the Heroku application. These are not looked at by the pwa, but `MAILGUN_SMTP_LOGIN` and `MAILGUN_SMTP_PASSWORD` can be used to populate `OR_SMTP_USER` and `OR_SMTP_PASSWORD` respectively.

The project can be deployed to Heroku using either the Docker image or by pushing the repository using Git.

### Docker

[Reference](https://devcenter.heroku.com/articles/container-registry-and-runtime)

First log in to the image repository using `heroku container:login`, which will use existing Heroku credentials.

The image can be built and pushed to the repository using the Heroku cli tool: `heroku container:push web`

### Git

The pwa project contains a submodule, which is configured using relative urls (required for Gitlab CI/CD). Heroku does not know how to initialize the submodule, so it must be removed from the project (`.gitmodules` file) before pushing.

Example command to push to Heroku: `git push -f https://heroku:$HEROKU_API_KEY@git.heroku.com/<app name>.git <local branch>:master` where `app name` and `local branch` should be replaced.