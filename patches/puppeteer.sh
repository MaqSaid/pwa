#! bin/bash

echo "patching puppeteer..."
grep -lr "status: number;" ./node_modules/@types/puppeteer | xargs sed -i'.bak' 's/status: number;/status(): number/g'