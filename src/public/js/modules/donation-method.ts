import {
  client as braintree,
  hostedFields,
  googlePayment as gpay,
  paypalCheckout as payPal,
  applePay
} from 'braintree-web';

import * as payPalCheckout from 'paypal-checkout';

import { validateFields } from './onerelief';
import { localAmt } from './donation';
import { showLoader, hideLoader } from './loader';

const currency = 'USD';
const token = (document.getElementById('bt-token') as HTMLInputElement).value;

let paymentComplete: boolean = false;

const addPaymentButton = (button: HTMLElement) => {
  const d = document.getElementById('payment-buttons');
  d.appendChild(button);
};

function getErrorDiv() {
  return document.getElementById('payment-errors');
}

const setErrorMess = (mess: string) => {
  getErrorDiv().innerHTML = `<div style="padding: 1em;">${mess}</div>`;
};

function clearErrorMess() {
  getErrorDiv().innerHTML = '';
}

function clearErrors() {
  clearErrorMess();
  clearCCErrors();
}

function payButton(btnClass: string, click: any, title = '') {
  const el = document.createElement('button');
  el.setAttribute('style', 'display: inline-block;');
  el.setAttribute('title', title);
  el.classList.add(btnClass);
  el.onclick = click;
  return el;
}
const postPayment = (
  fields: { amount: number; name?: string; email?: string },
  nonce: string,
  mess: string
) => {
  const data = {
    ...fields,
    nonce
  };
  const loaderDiv = 'payment-method';
  showLoader(loaderDiv);
  return fetch(`${window.location}`, {
    headers: {
      'Content-Type': 'application/json'
    },
    method: 'POST',
    body: JSON.stringify(data)
  })
    .then((resp: Response) => {
      hideLoader(loaderDiv);
      if (resp.ok) {
        window.debug('payment complete, moving to success page');
        paymentComplete = true;
        document
          .getElementsByClassName('donation-content')[0]
          .dispatchEvent(new Event('paymentcomplete'));
      } else {
        console.error(resp.statusText);
        console.error(resp.body);
        setErrorMess(mess);
      }
    })
    .catch(err => {
      hideLoader(loaderDiv);
      throw err;
    });
};

// Apple Pay

const payWithApplePay = (appleClient: any) => {
  const mess = 'There was a problem using Apple Pay.';
  return () => {
    const paymentRequest = appleClient.createPaymentRequest({
      requiredShippingContactFields: ['name', 'email'],
      total: {
        label: 'OneRelief',
        amount: `${localAmt()}`
      }
    });

    clearErrors();
    clearCCForm();

    let appleSession: any;
    // Will throw if hosting on an insecure site or invalid
    // parameters have been passed to the request.
    try {
      appleSession = new window.ApplePaySession(2, paymentRequest);
    } catch (e) {
      window.debug(e);
    }

    // Setup callbacks for the Apple Session
    appleSession.onvalidatemerchant = (event: any) => {
      appleClient.performValidation(
        {
          validationURL: event.validationURL,
          displayName: 'OneRelief'
        },
        (validationErr: any, merchantSession: any) => {
          if (validationErr) {
            setErrorMess(mess);
            console.error('Error validating merchant:');
            console.error(validationErr);
            appleSession.abort();
            return;
          }
          appleSession.completeMerchantValidation(merchantSession);
        }
      );
    };

    appleSession.onpaymentauthorized = (event: any) => {
      const shippingContact = event.payment.shippingContact;
      appleClient.tokenize(
        {
          token: event.payment.token
        },
        (tokenizeErr: any, payload: any) => {
          if (tokenizeErr) {
            console.error('Error tokenizing Apple Pay:', tokenizeErr);
            appleSession.completePayment(window.ApplePaySession.STATUS_FAILURE);
            return;
          }
          appleSession.completePayment(window.ApplePaySession.STATUS_SUCCESS);
          return postPayment(
            {
              amount: localAmt(),
              email: shippingContact.emailAddress,
              name: `${shippingContact.givenName} ${shippingContact.familyName}`
            },
            payload.nonce,
            mess
          ).catch((err: any) => {
            setErrorMess(mess);
            console.error(err);
          });
        }
      );
    };

    // must be called in onclick (user initiated), or Apple will get you
    appleSession.begin();
  };
};

const initApplePay = () => {
  const paySession = window.ApplePaySession;
  if (paySession && paySession.canMakePayments()) {
    window.debug('Apply Pay is supported');
    return braintree
      .create({ authorization: token })
      .then((client: any) => {
        return applePay.create({
          client
        });
      })
      .then((instance: any) => {
        window.debug(
          `Apple Pay merchant identifier: ${instance.merchantIdentifier}`
        );
        return paySession
          .canMakePaymentsWithActiveCard(instance.merchantIdentifier)
          .then((success: boolean) => {
            if (success) {
              addPaymentButton(
                payButton('applepay', payWithApplePay(instance), 'Apple Pay')
              );
            } else {
              window.debug(
                'unable to make Apple Pay payments, perhaps no active card?'
              );
            }
          });
      })
      .catch((err: BrainTreeError) => {
        console.error(err);
      });
  } else {
    window.debug('Apple Pay not available');
  }
};

// Google Pay

const payWithGPay = (
  gPayClient: any,
  paymentsClient: any,
  merchantId: string
) => {
  return () => {
    const mess = 'There was a problem using Google Pay.';
    const merchantIdOrUndefined = merchantId !== '' ? merchantId : undefined;
    const paymentDataRequest = gPayClient.createPaymentDataRequest({
      merchantId: merchantIdOrUndefined,
      emailRequired: true,
      transactionInfo: {
        currencyCode: 'USD',
        totalPriceStatus: 'FINAL',
        totalPrice: localAmt()
      }
    });

    let customerEmail: string;
    let customerName: string;
    paymentsClient
      .loadPaymentData(paymentDataRequest)
      .then((paymentData: any) => {
        customerEmail = paymentData.email;
        const customerAddress = paymentData.cardInfo.billingAddress;
        customerName = customerAddress
          ? customerAddress.name
          : customerEmail.substring(0, customerEmail.indexOf('@'));
        return gPayClient.parseResponse(paymentData);
      })
      .then((payload: any) => {
        return postPayment(
          { amount: localAmt(), email: customerEmail, name: customerName },
          payload.nonce,
          mess
        );
      })
      .catch((err: any) => {
        setErrorMess(mess);
        console.error(err);
      });
  };
};

const initGPay = (braintreeEnv: string, merchantId: string) => {
  if (window.google) {
    const paymentsClient = new window.google.payments.api.PaymentsClient({
      environment: braintreeEnv === 'sandbox' ? 'TEST' : 'PRODUCTION'
    });
    let braintreeGPay: any = null;

    return braintree
      .create({ authorization: token })
      .then((client: any) => {
        return gpay.create({
          client
        });
      })
      .then((instance: any) => {
        braintreeGPay = instance;
        return paymentsClient.isReadyToPay({
          allowedPaymentMethods: instance.createPaymentDataRequest()
            .allowedPaymentMethods
        });
      })
      .then((response: any) => {
        if (response.result) {
          window.debug('Google Pay is supported');
          addPaymentButton(
            payButton(
              'googlepay',
              payWithGPay(braintreeGPay, paymentsClient, merchantId),
              'Google Pay'
            )
          );
        } else {
          window.debug('Google Pay not available');
        }
      })
      .catch((err: BrainTreeError) => {
        console.error(err);
      });
  } else {
    window.debug('Google Pay not available');
  }
};

// PayPal

function initPayPal(braintreeEnv: string) {
  const mess = 'There was a problem using PayPal.';
  const renderProps = (instance: any) => {
    return {
      env: braintreeEnv,
      payment: () => {
        window.debug('pressed paypal button');
        clearErrors();
        clearCCForm();
        return instance.createPayment({
          flow: 'checkout',
          amount: localAmt(),
          currency
        });
      },
      onAuthorize: (data: any, actions: any) => {
        instance
          .tokenizePayment(data)
          .then((payload: any) => {
            return postPayment(
              {
                name: `${payload.details.firstName} ${
                  payload.details.lastName
                }`,
                email: payload.details.email,
                amount: localAmt()
              },
              payload.nonce,
              mess
            );
          })
          .catch((err: any) => {
            setErrorMess(mess);
            console.error(err);
          });
      },
      onCancel: () => {
        window.debug('PayPal canceled');
      },
      onError: (err: any) => {
        setErrorMess(mess);
        console.error(err);
      }
    };
  };
  return braintree
    .create({ authorization: token })
    .then((client: any) => {
      return payPal.create({
        client
      });
    })
    .then((payPalInstance: any) => {
      return payPalCheckout.Button.render(
        renderProps(payPalInstance),
        '#paypal-button'
      );
    })
    .catch((err: any) => {
      console.error('error setting up Braintree PayPal client');
      console.error(err);
    });
}

// Credit Cards

// styles need to be injected into braintree iframe hosted fields
const hostedStyles = {
  'input.invalid': {
    color: 'red'
  },
  'input.valid': {}
};

const hostedErrorMessages: any = {
  'expiration-date': 'Please enter a valid expiry date',
  cvv: 'Please enter a valid CVV value',
  number: 'Please enter a valid card number',
  email: 'Please enter a valid email address',
  fullname: 'Please enter a name'
};

function getCCForm() {
  return document.getElementById('credit-card-info') as HTMLFormElement;
}

function clearCCForm() {
  clearHostedFields();
  const form = getCCForm();
  form.reset();
}

function clearCCErrors() {
  const form = getCCForm();
  Array.from(form.getElementsByClassName('validation-message')).forEach(
    (e: HTMLElement) => {
      e.textContent = '';
    }
  );
}

function setFieldError(label: HTMLLabelElement, text: string) {
  label.lastChild.textContent = text;
}

function validateCCForm(hostedInstance: any) {
  const nameInput = document.querySelector(
    '#credit-card-info input[id="fullname"]'
  ) as HTMLInputElement;
  const emailInput = document.querySelector(
    '#credit-card-info input[id="email"]'
  ) as HTMLInputElement;
  const state = hostedInstance.getState();

  let hostedErrors = false;
  Object.keys(state.fields).forEach(key => {
    const fieldState = state.fields[key];
    if (fieldState.isValid) {
      return;
    }
    const id = fieldState.container.id;
    setFieldError(
      document.querySelector(`label[for="${id}"]`) as HTMLLabelElement,
      hostedErrorMessages[id]
    );
    hostedErrors = true;
  });

  const constraints = {
    email: {
      email: {
        message: hostedErrorMessages[emailInput.id],
        allowEmpty: false
      }
    },
    name: {
      presence: {
        message: hostedErrorMessages[nameInput.id],
        allowEmpty: false
      }
    }
  };

  const validationRes = validateFields(
    {
      email: emailInput.value,
      name: nameInput.value
    },
    constraints,
    {
      email: (mess: string) =>
        setFieldError(emailInput.parentNode as HTMLLabelElement, mess),
      name: (mess: string) =>
        setFieldError(nameInput.parentNode as HTMLLabelElement, mess)
    }
  );
  const customErrors = validationRes ? true : false;

  return !customErrors && !hostedErrors;
}

function initCCHostedFields() {
  const problemMess = 'There was a problem making the Credit Card payment.';

  const submitButton = document.getElementById('submit-button');
  return braintree
    .create({ authorization: token })
    .then((client: any) => {
      return hostedFields.create({
        client,
        styles: hostedStyles,
        fields: {
          number: {
            selector: '#number',
            placeholder: '---- ---- ---- ----'
          },
          cvv: {
            selector: '#cvv',
            placeholder: '---'
          },
          expirationDate: {
            selector: '#expiration-date',
            placeholder: 'MM/YY'
          }
        }
      });
    })
    .then((hostedInstance: any) => {
      submitButton.classList.remove('hidden');
      submitButton.removeAttribute('disabled');

      const form = getCCForm();
      form.addEventListener(
        'submit',
        event => {
          event.preventDefault();
          clearErrors();

          if (!validateCCForm(hostedInstance)) {
            return;
          }

          showLoader('submit-button');
          hostedInstance
            .tokenize()
            .then((payload: any) => {
              hideLoader('submit-button');
              return postPayment(
                {
                  amount: localAmt(),
                  name: form.fullname.value,
                  email: form.email.value
                },
                payload.nonce,
                problemMess
              );
            })
            .catch((err: any) => {
              hideLoader('submit-button');
              setErrorMess(problemMess);
              console.error(err);
            });
        },
        false
      );
      return hostedInstance;
    })
    .catch((err: any) => {
      console.error(err);
    });
}

let clearHostedFields = () => {
  console.warn('hosted fields not created, unable to clear');
};

const init = (googleMerchantId: string, braintreeEnv: string) => {
  const paymentButtons = 'payment-buttons';
  showLoader(paymentButtons);
  Promise.all([
    initGPay(braintreeEnv, googleMerchantId),
    initApplePay(),
    initPayPal(braintreeEnv),
    initCCHostedFields().then((hostedInstance: any) => {
      clearHostedFields = () => {
        hostedInstance.clear('cvv');
        hostedInstance.clear('number');
        hostedInstance.clear('expirationDate');
      };
    })
  ])
    .then(() => {
      hideLoader(paymentButtons);
    })
    .catch(err => {
      hideLoader(paymentButtons);
    });
};

(window as any).donationMethod = { init, paymentComplete };
