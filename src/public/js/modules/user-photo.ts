import { dataURItoBlob, donationId, getOrientation } from './helpers';

const getElement = (el: string) => document.getElementById(el);
const canvas = getElement('shareCanvas') as HTMLCanvasElement;
const canvas2 = getElement('shareCanvas2') as HTMLCanvasElement;
const context = canvas.getContext('2d');
const context2 = canvas2.getContext('2d');
context.clearRect(0, 0, canvas.width, canvas.height);
context2.clearRect(0, 0, canvas.width, canvas.height);
const overlayIMG = '/images/social/ihelpedrohrefugeees.svg';
const overlayIMG02 = '/images/social/iamareliefwarrior.svg';
const width = 1080;
const height = 1080;
const imageObj = new Image();
const overlay = new Image();
let url = '';

const inputElement = getElement('share3') as HTMLInputElement;
const inputElement2 = getElement('share4') as HTMLInputElement;
const share3 = getElement('share3-label');
const share4 = getElement('share4-label');
const closeButton = getElement('close-button');
const closeButton2 = getElement('close-button2');

inputElement.addEventListener('change', handleFiles, false);
inputElement2.addEventListener('change', handleFiles, false);
closeButton.addEventListener(
  'click',
  event => clearImage(event, 'share3-label'),
  false
);
closeButton2.addEventListener(
  'click',
  event => clearImage(event, 'share4-label'),
  false
);

function init(apiUrl: any) {
  url = apiUrl;
}

function resetOrientation(
  element: string,
  srcBase64: any,
  srcOrientation: number,
  callback: any,
  overlayImage: string,
  targetCanvas: HTMLCanvasElement,
  targetContext: any
) {
  if (element === 'share3-label') {
    inputElement.disabled = true;
  } else {
    inputElement2.disabled = true;
  }

  const fitImageOn = () => {
    const wRatio = targetCanvas.width / imageObj.width;
    const hRatio = targetCanvas.height / imageObj.height;
    const ratio = Math.max(wRatio, hRatio);

    targetContext.drawImage(
      imageObj,
      0,
      0,
      imageObj.width,
      imageObj.height,
      0,
      0,
      imageObj.width * ratio,
      imageObj.height * ratio
    );
  };

  imageObj.onload = () => {
    overlay.src = overlayImage;
    overlay.onload = () => {
      // transform context before drawing image
      switch (srcOrientation) {
        case 2:
          targetContext.transform(-1, 0, 0, 1, width, 0);
          break;
        case 3:
          targetContext.transform(-1, 0, 0, -1, width, height);
          break;
        case 4:
          targetContext.transform(1, 0, 0, -1, 0, height);
          break;
        case 5:
          targetContext.transform(0, 1, 1, 0, 0, 0);
          break;
        case 6:
          targetContext.transform(0, 1, -1, 0, height, 0);
          break;
        case 7:
          targetContext.transform(0, -1, -1, 0, height, width);
          break;
        case 8:
          targetContext.transform(0, -1, 1, 0, 0, width);
          break;
        default:
          break;
      }

      // draw image
      fitImageOn();

      switch (srcOrientation) {
        case 2:
          targetContext.transform(1, 0, 0, -1, 0, 0, width);
          break;
        case 3:
          targetContext.transform(-1, 0, 0, -1, height, width);
          break;
        case 4:
          targetContext.transform(-1, 0, 0, 1, 0, width);
          break;
        case 5:
          targetContext.transform(0, 1, 1, 0, 0, 0);
          break;
        case 6:
          targetContext.transform(0, -1, 1, 0, 0, height);
          break;
        case 7:
          targetContext.transform(0, -1, -1, 0, width, height);
          break;
        case 8:
          targetContext.transform(0, 1, -1, 0, 0, height);
          break;
        default:
          break;
      }

      targetContext.drawImage(overlay, 0, 0, 1080, 1080);

      callback(targetCanvas.toDataURL('image/jpeg'));
    };
  };

  imageObj.src = srcBase64;
}

function clearImage(event: any, element: any) {
  event.preventDefault();
  if (element === 'share3-label') {
    sessionStorage.removeItem('3');
    context.clearRect(0, 0, canvas.width, canvas.height);
    closeButton.style.visibility = 'hidden';
    share3.style.backgroundImage = `url("${'/images/social/upload.png'}"), url("${'/images/social/share-bg01.jpg'}")`;
    share3.classList.remove('selector');
    inputElement.addEventListener('change', handleFiles, false);
    inputElement.removeEventListener('click', uploadLink, false);
    inputElement.disabled = false;
    inputElement.type = 'file';
  }
  if (element === 'share4-label') {
    sessionStorage.removeItem('4');
    context2.clearRect(0, 0, canvas2.width, canvas2.height);
    closeButton2.style.visibility = 'hidden';
    share4.style.backgroundImage = `url("${'/images/social/upload.png'}"), url("${'/images/social/share-bg02.jpg'}")`;
    share4.classList.remove('selector');
    inputElement2.addEventListener('change', handleFiles, false);
    inputElement2.removeEventListener('click', uploadLink, false);
    inputElement2.disabled = false;
    inputElement2.type = 'file';
  }
}

function handleFiles(event: any) {
  event.preventDefault();
  const file = event.target.files[0];
  const imageURL = URL.createObjectURL(file);
  const id = event.srcElement.id;
  if (id === 'share3') {
    inputElement.removeEventListener('change', handleFiles, false);
    getOrientation(file, (orientation: number) => {
      resetOrientation(
        'share3-label',
        imageURL,
        orientation,
        (jpeg: any) => {
          uploadImageAction(jpeg, 3)
            .then(success => {
              preview('share3-label', overlayIMG, jpeg);
              inputElement.addEventListener('click', uploadLink, false);
              inputElement.type = 'radio';
              inputElement.disabled = false;
            })
            .catch(error => console.log(error));
        },
        overlayIMG,
        canvas,
        context
      );
    });
  } else {
    inputElement2.removeEventListener('change', handleFiles, false);
    getOrientation(file, (orientation: number) => {
      resetOrientation(
        'share4-label',
        imageURL,
        orientation,
        (jpeg: any) => {
          uploadImageAction(jpeg, 4)
            .then(success => {
              preview('share4-label', overlayIMG02, jpeg);
              inputElement2.addEventListener('click', uploadLink, false);
              inputElement2.type = 'radio';
              inputElement2.disabled = false;
            })
            .catch(error => console.log(error));
        },
        overlayIMG02,
        canvas2,
        context2
      );
    });
  }
}

function preview(element: any, overlayImage: any, file: any) {
  const bgInput = document.getElementById(element);
  bgInput.style.backgroundImage = `url("${overlayImage}"), url("${file}")`;
  bgInput.className = 'selector';
  if (element === 'share3-label') {
    closeButton.style.visibility = 'visible';
  } else {
    closeButton2.style.visibility = 'visible';
  }
}

function uploadLink(event: any) {
  const id = event.target.id === 'share3' ? '3' : '4';
  const imageLink =
    event.target.id === 'share3'
      ? sessionStorage.getItem('3')
      : sessionStorage.getItem('4');
  const options = {
    method: 'PUT',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      id,
      donationId: donationId()
    })
  };
  fetch(`${window.location}`, options)
    .then(res => res.json())
    .then(json => {
      (getElement('insta-button') as HTMLAnchorElement).href = imageLink;
      (getElement('insta-button') as HTMLAnchorElement).removeAttribute(
        'download'
      );
      (getElement('insta-button') as HTMLAnchorElement).target = '_blank';
      (getElement(
        'email-button'
      ) as HTMLAnchorElement).href = `mailto:?subject=${
        json.emailSubject
      }&body=${json.emailBody}${imageLink}`;
      (getElement(
        'twit-button'
      ) as HTMLAnchorElement).href = `https://twitter.com/intent/tweet?url=${
        json.shareUrl
      }`;
      (getElement(
        'socialdownload-button'
      ) as HTMLAnchorElement).href = imageLink;
      (getElement(
        'socialdownload-button'
      ) as HTMLAnchorElement).removeAttribute('download');
      (getElement('socialdownload-button') as HTMLAnchorElement).target =
        '_blank';
    });
}

function uploadImageAction(image: any, id: any) {
  const convertedImage = dataURItoBlob(image);
  const data = new FormData();

  data.append('donationId', donationId());
  if (convertedImage) {
    data.append('share', convertedImage);
  }
  const options = {
    method: 'POST',
    body: data
  };

  return fetch(`${url}/donations/${donationId()}/share`, options)
    .then(res => res.json())
    .then(json => {
      sessionStorage.setItem(id, json.url);
    })
    .catch(err => {
      console.log('Upload failed');
      console.log(err);
    });
}

(window as any).share = { init };
