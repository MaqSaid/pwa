interface LoaderRequestInit extends RequestInit {
  loader: string;
}

const oldFetch = fetch;

function fetchLoader(
  input: RequestInfo,
  init?: LoaderRequestInit
): Promise<Response> {
  if (!init || !init.loader) {
    throw new Error('fetchLoader requires a loader init property');
  }
  const fetchCall = oldFetch(input, init);

  showLoader(init.loader);
  fetchCall
    .then(() => {
      hideLoader(init.loader);
    })
    .catch(() => {
      hideLoader(init.loader);
    });

  return fetchCall;
}

function hideLoader(loaderId: string) {
  const el = document.getElementById(loaderId);
  el.classList.remove('show');
  Array.from(el.querySelectorAll(`#${loaderId} > .loader`)).forEach(loaderEl =>
    loaderEl.classList.remove('show')
  );
}

function showLoader(loaderId: string) {
  const el = document.getElementById(loaderId);
  el.classList.add('show');
  Array.from(el.querySelectorAll(`#${loaderId} > .loader`)).forEach(loaderEl =>
    loaderEl.classList.add('show')
  );
}

export { fetchLoader, showLoader, hideLoader };
