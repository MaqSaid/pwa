function onLoadFunction() {
  /*initialize cookie banner event listener*/
  document.getElementById('cookieButton').addEventListener('click', () => {
    document.cookie = 'userAcceptedCookieBanner=True';

    const element = document.getElementById('cookie-banner');
    element.innerHTML = `<span style="color: #ffffff">Thank You!</span>`;

    const timeout = 700;
    setTimeout(() => {
      element.style.opacity = '0';
    }, timeout);
    setTimeout(() => {
      element.parentNode.removeChild(element);
    }, timeout + 500);
  });
}
window.onload = onLoadFunction;
