import './loader';
import { validate } from 'validate.js';

interface MessageSetters {
  [key: string]: (mess: string) => void;
}

// small abstraction over the validate.js validate function that takes a map
// of functions to call when reporting validation errors. currently just supports
// setting a single message.
function validateFields(
  values: any,
  validation: any,
  messageSetters: MessageSetters
) {
  const valResult: any[] = validate(values, validation);
  if (!valResult) {
    return;
  }
  Object.keys(valResult).forEach((key: any) => {
    // remove the name of the field from the start of the message.
    // not configurable in validate.js
    const mess: string = valResult[key][0];
    messageSetters[key](mess.substring(key.length, mess.length));
  });
  return valResult;
}

const ua = navigator.userAgent.toLowerCase();

const isAndroid = ua.indexOf('android') > -1;
const isiPhone = ua.match(/iphone/i) != null;
const isiPad = ua.match(/ipad/i) != null;

export { validateFields, isAndroid, isiPhone, isiPad };
export * from './loader';
