// convert base64 image to jpeg format
export function dataURItoBlob(dataURI: any) {
  const byteString = (() => {
    if (dataURI.split(',')[0].indexOf('base64') >= 0) {
      return atob(dataURI.split(',')[1]);
    } else {
      return unescape(dataURI.split(',')[1]);
    }
  })();

  const mimeString = dataURI
    .split(',')[0]
    .split(':')[1]
    .split(';')[0];
  const ia = new Uint8Array(byteString.length);

  for (let i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i);
  }

  return new Blob([ia], { type: mimeString });
}

// get donation URL
export function donationId() {
  const location = window.location.pathname;
  return location.split('/')[2];
}

// get image orientation for re-orientation
export function getOrientation(file: File, callback: any) {
  const reader = new FileReader();

  reader.onload = (event: ProgressEvent) => {
    if (!event.target) {
      return;
    }

    const fileReader = event.target as FileReader;
    const view = new DataView(fileReader.result as ArrayBuffer);

    if (view.getUint16(0, false) !== 0xffd8) {
      return callback(-2);
    }

    const length = view.byteLength;
    let offset = 2;

    while (offset < length) {
      if (view.getUint16(offset + 2, false) <= 8) {
        return callback(-1);
      }
      const marker = view.getUint16(offset, false);
      offset += 2;

      if (marker === 0xffe1) {
        if (view.getUint32((offset += 2), false) !== 0x45786966) {
          return callback(-1);
        }

        const little = view.getUint16((offset += 6), false) === 0x4949;
        offset += view.getUint32(offset + 4, little);
        const tags = view.getUint16(offset, little);
        offset += 2;
        for (let i = 0; i < tags; i++) {
          if (view.getUint16(offset + i * 12, little) === 0x0112) {
            return callback(view.getUint16(offset + i * 12 + 8, little));
          }
        }
      } else if ((marker && 0xff00) !== 0xff00) {
        break;
      } else {
        offset += view.getUint16(offset, false);
      }
    }
    return callback(-1);
  };

  reader.readAsArrayBuffer(file);
}
