import './onerelief';

const amountKey = 'donationAmount';
const localAmt = (amount?: number): number => {
  if (amount) {
    localStorage.setItem(amountKey, JSON.stringify(amount));
    return;
  }
  return JSON.parse(localStorage.getItem(amountKey));
};

export { localAmt };
