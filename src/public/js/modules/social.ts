import { isAndroid, isiPad, isiPhone } from './onerelief';

function fbShare(shareUrl: string) {
  window.debug('sharing link with FB');
  window.FB.ui(
    {
      method: 'share',
      href: shareUrl
    },
    (response: any): void => {
      return undefined;
    }
  );
}

function init(fbId: string) {
  initFb(fbId);
  initShareButtons();
  window.onload = initialSelection;
}

function initialSelection() {
  // select initial radio button. done in js to handle selection caching.
  const radios: HTMLInputElement[] = Array.from(
    document.querySelectorAll('input[type=radio]')
  );
  const click = new MouseEvent('click', {
    view: window,
    bubbles: false,
    cancelable: true
  });
  radios[0].dispatchEvent(click);
}

function initFb(fbId: string) {
  // FB sdk setup code
  // https://developers.facebook.com/docs/javascript/quickstart#loading
  window.fbAsyncInit = () => {
    window.debug(`init FB sdk with app id ${fbId}`);
    window.FB.init({
      appId: fbId,
      autoLogAppEvents: true,
      xfbml: true,
      version: 'v2.12'
    });
  };

  ((d, s, id) => {
    const fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
      return;
    }
    const js: any = d.createElement(s);
    js.id = id;
    js.src = 'https://connect.facebook.net/en_US/sdk.js';
    fjs.parentNode.insertBefore(js, fjs);
  })(document, 'script', 'facebook-jssdk');
}

function initShareButtons() {
  if (isAndroid || isiPad || isiPhone) {
    const waButton = document.getElementsByClassName(
      'wa-button'
    )[0] as HTMLElement;
    waButton.classList.remove('hidden');
  }
}

(window as any).social = { init, initShareButtons, fbShare };
