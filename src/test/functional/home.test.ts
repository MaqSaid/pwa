import * as request from 'supertest';
import { app } from '../../server/app';

describe('GET /', () => {
  it('should return 304 if onboarding complete not complete', done => {
    request(app)
      .get('/')
      .expect(302, done);
  });
});
