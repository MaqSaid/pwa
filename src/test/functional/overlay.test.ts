import * as overlay from '../../server/modules/overlay';
import * as fs from 'fs';

describe('apply an overlay to a donor uploaded image to create a new composite image', () => {
  it(
    'should return the file path of a new image',
    done => {
      (async () => {
        overlay
          .applyOverlay(
            `${__dirname}/donorimages/test_donor_image.jpeg`,
            overlay.IHELPEDROHREGUGEES_OVERLAY
          )
          .then(newImageFile => {
            expect(newImageFile).toBeTruthy();
            // Clean up: delete the new image file
            fs.unlink(newImageFile, done);
          })
          .catch(error => {
            done(error);
          });
      })();
    },
    15000
  );
});
