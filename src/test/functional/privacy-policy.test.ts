import * as request from 'supertest';
import { app } from '../../server/app';

describe('GET /privacy', () => {
  it('should return 200 OK', done => {
    request(app)
      .get('/privacy')
      .expect(200, done);
  });
});
