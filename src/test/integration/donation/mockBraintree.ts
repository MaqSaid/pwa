jest.mock('braintree');
import * as braintree from 'braintree';

const mockClientToken = {
  clientToken: {
    generate: (opts: any, callback: (err: Error, succ: object) => void) => {
      callback(undefined, { clientToken: '' });
    }
  }
};

const mock = () => braintree.connect.mockImplementation(() => mockClientToken);

export { mock };
