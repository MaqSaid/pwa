interface Window {
  env: string;
  $script: $script;
  debug: (mess: any) => void;
  ApplePaySession?: any;
  FB: any;
  fbAsyncInit: () => void;
  google?: any;
}
