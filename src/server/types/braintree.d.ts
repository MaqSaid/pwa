declare module 'braintree';

declare interface BrainTreeError {
  message: string;
}
