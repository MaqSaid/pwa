import { Request, Response } from 'express';

/**
 * GET /
 * Privacy Policy page.
 */
export let index = (req: Request, res: Response) => {
  res.render('privacy', {
    title: 'Privacy Policy'
  });
};
