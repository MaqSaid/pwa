import * as url from 'url';
import { Request, Response } from 'express';
import * as braintree from 'braintree';
import fetch, * as nf from 'node-fetch';

/*
const TRANSACTION_SUCCESS_STATUSES = [
  braintree.Transaction.Status.Authorizing,
  braintree.Transaction.Status.Authorized,
  braintree.Transaction.Status.Settled,
  braintree.Transaction.Status.Settling,
  braintree.Transaction.Status.SettlementConfirmed,
  braintree.Transaction.Status.SettlementPending,
  braintree.Transaction.Status.SubmittedForSettlement
];
*/

const envMap: any = {
  sandbox: braintree.Environment.Sandbox,
  production: braintree.Environment.Production
};

let gateway: any;
try {
  gateway = braintree.connect({
    environment: envMap[process.env.OR_BRAINTREE_ENV],
    merchantId: process.env.OR_BRAINTREE_MERCHANT,
    publicKey: process.env.OR_BRAINTREE_PUBLICKEY,
    privateKey: process.env.OR_BRAINTREE_PRIVATEKEY
  });
} catch (err) {
  console.error(err);
  // only throw for non-developer environments
  if (process.env.NODE_ENV === 'production') {
    throw err;
  }
}

let id: any;

export const index = (req: Request, res: Response) => {
  gateway.clientToken.generate({}, (err: BrainTreeError, response: any) => {
    if (err) {
      console.error(err.message);
      res.sendStatus(500);
      return;
    }
    res.render('donation', {
      title: 'Donation',
      clientToken: response.clientToken
    });
  });
};

export const value = (req: Request, res: Response) => {
  res.render('donation/amount-partial', {});
};

export const method = (req: Request, res: Response) => {
  gateway.clientToken.generate({}, (err: BrainTreeError, response: any) => {
    if (err) {
      console.error(err.message);
      res.sendStatus(500);
      return;
    }
    res.render('donation/method-partial', {
      clientToken: response.clientToken,
      amount: req.query.donationAmount
    });
  });
};

export const checkout = (req: Request, res: Response) => {
  const campaignId = req.params.id;
  const nonce = req.body.nonce;
  const amount = req.body.amount;
  const donorDetails = {
    name: req.body.name,
    email: req.body.email
  };

  console.info(
    `received donation of ${amount} for campaign ${campaignId} from ${
      donorDetails.name
    }`
  );

  let braintreeSuccess = false;
  gateway.transaction
    .sale({
      amount,
      paymentMethodNonce: nonce,
      options: {
        submitForSettlement: true
      }
    })
    .then((result: any) => {
      console.log(result);
      if (result.success !== true) {
        throw Error(result.message);
      }
      const tx = result.transaction;
      console.log(tx);
      braintreeSuccess = true;
      // details of the donation need to be stored in the backend
      const donation = {
        amount: tx.amount,
        method: tx.paymentInstrumentType,
        paymentId: tx.id,
        paymentMethod: tx.paymentInstrumentType,
        campaignId,
        donorDetails,
        madeAt: tx.createdAt
      };
      if (tx.id) {
        id = tx.id;
      }
      const donationsUrl = url.resolve(
        process.env.OR_API,
        `campaigns/${campaignId}/donations`
      );
      return fetch(donationsUrl, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(donation)
      });
    })
    .then((resp: nf.Response) => {
      if (!resp.ok) {
        // TODO ring some alarm bells
        console.error(
          `got bad response code ${resp.status} from donation endpoint`
        );
        resp.text().then(text => console.error(text));
      }
      res.sendStatus(200);
    })
    .catch((err: Error) => {
      if (braintreeSuccess) {
        console.error(`error posting to donation endpoint`, err);
        res.send();
        return;
      }
      console.error('error sending payment transaction to Braintree', err);
      res.sendStatus(500);
    });
};

export const success = (req: Request, res: Response) => {
  const options = {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    method: 'POST',
    body: JSON.stringify({ id })
  };
  const getIdUrl = url.resolve(process.env.OR_API, `donations/getId`);
  return fetch(`${getIdUrl}`, options)
    .then(response => response.json())
    .then(json => {
      res.render('donation/success', {
        donationId: json
      });
    })
    .catch(error => {
      console.error(error);
      res.sendStatus(500);
    });
};

export const share = (req: Request, res: Response) => {
  // TODO validate the donation id
  const data: any = {};
  data.xhr = req.headers['X-Requested-With'] === 'XMLHttpRequest';
  res.render('donation/share', data);
};
