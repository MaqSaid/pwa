import { Request, Response } from 'express';

/**
 * GET /
 * About Us page.
 */
export let index = (req: Request, res: Response) => {
  res.render('about', {
    title: 'About'
  });
};
