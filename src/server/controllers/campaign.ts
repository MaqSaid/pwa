import { Request, Response } from 'express';
import * as moment from 'moment';

import { api } from '../api';
import { staticShares } from '../modules/social';
import { getAbsoluteUrl } from './helpers';

function socialShareOgInfo(campaign: Campaign, picId: string) {
  return {
    ogImage: getAbsoluteUrl(staticShares[picId]),
    ogTitle: `I helped out with ${campaign.title}`,
    ogDescription: `👉I made a small donation towards ${
      campaign.title
    } with the help of #OneRelief. Join me at at one-relief.org 😇`
  };
}

const index = (req: Request, resp: Response) => {
  const campaignUrl = getAbsoluteUrl(req.url);
  api
    .getCampaignDetails(req.params.id)
    .then((campaign: Campaign) => {
      // handle requests from social sharing
      const data = req.query.p ? socialShareOgInfo(campaign, req.query.p) : {};
      resp.render('campaign', {
        ...data,
        ogUrl: campaignUrl,
        campaignUrl: `/campaigns/${campaign.id}`,
        title: campaign.title,
        campaign,
        moment
      });
    })
    .catch((err: Error) => {
      console.log(err);
      resp.sendStatus(500);
    });
};

export { index };
