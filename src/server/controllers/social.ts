import { Request, Response } from 'express';

import { getAbsoluteUrl } from './helpers';
import { api } from '../api';
import { staticShares, emailBody, emailSubject } from '../modules/social';

function getImageUrl(id: string) {
  return getAbsoluteUrl(staticShares[id]);
}

function getCampaignUrl(id: string) {
  return getAbsoluteUrl(`/campaigns/${id}`);
}

function getTemplateData(id: string, imageId: string): Promise<any> {
  return api.getCampaignDetails(id).then((campaign: Campaign) => {
    const campaignUrl = getCampaignUrl(id);
    const shareUrl = `${campaignUrl}?p=${imageId}`;
    return {
      campaign,
      campaignUrl:
        imageId === '3' || imageId === '4'
          ? `${getCampaignUrl(id)}`
          : campaignUrl,
      shareUrl:
        imageId === '3' || imageId === '4' ? `${getCampaignUrl(id)}` : shareUrl,
      imageId,
      imageUrl:
        imageId === '3' || imageId === '4' ? null : getImageUrl(imageId),
      campaignImageUrl: campaign.details.images.main.url,
      emailSubject: emailSubject(campaign),
      emailBody:
        imageId === '3' || imageId === '4'
          ? emailBody(campaign, `${getCampaignUrl(id)}`)
          : emailBody(campaign, shareUrl)
    };
  });
}

// handles initial requests for the social sharing page
export const share = (req: Request, res: Response) => {
  const imageId = '1';
  let donation: Donation;
  api
    .getDonation(req.params.id)
    .then(json => {
      donation = json;
      return getTemplateData(donation.campaign, imageId);
    })
    .then((templateData: any) => {
      res.render('social/share', {
        ...templateData,
        title: 'Social Sharing',
        fbId: process.env.OR_FB_APP_ID,
        staticShares,
        donationId: req.params.id,
        apiUrl: process.env.OR_SITE
      });
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(500);
    });
};

// handles xhr requests to overlay a new photo
export const overlay = (req: Request, res: Response) => {
  res.sendStatus(200);
};

// handles xhr request to change the share image
export const select = (req: Request, res: Response) => {
  const campaignId = req.body.campaignId;
  const imageId = req.body.share;

  getTemplateData(campaignId, imageId)
    .then((data: any) => {
      res.render('social/social-buttons', {
        ...data,
        siteUrl: getAbsoluteUrl(req.url)
      });
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(500);
    });
};

export const customSelect = (req: Request, res: Response) => {
  let donation: Donation;
  const imageId = String(req.body.id);
  const donationId = req.body.donationId;

  api
    .getDonation(donationId)
    .then(json => {
      donation = json;
      return getTemplateData(donation.campaign, imageId);
    })
    .then((templateData: any) => {
      res.send({ ...templateData });
    });
};

export interface MulterFile {
  key?: string;
  location?: string;
  mimetype?: string;
}

export const userPhotos = (
  req: Request & { file: MulterFile },
  res: Response
) => {
  api
    .postImage({
      fileName: req.file.key,
      url: req.file.location,
      donationId: req.body.donationId
    })
    .then(json => {
      res.send(json);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(500);
    });
};
