import * as express from 'express';
import * as compression from 'compression';
import * as session from 'express-session';
import * as bodyParser from 'body-parser';
import * as logger from 'morgan';
import * as lusca from 'lusca';
import * as dotenv from 'dotenv';
import * as flash from 'express-flash';
import * as path from 'path';
import * as fs from 'fs';

import * as AWS from 'aws-sdk';
import * as multer from 'multer';
import * as multerS3 from 'multer-s3';

if (fs.existsSync(path.join(process.cwd(), '/.env'))) {
  dotenv.config({ path: '.env' });
} else {
  console.log(
    'ignoring missing .env file, values will be taken from shell env'
  );
}

// Controllers (route handlers)
import * as homeController from './controllers/home';
import * as aboutController from './controllers/about';
import * as volunteerController from './controllers/volunteer';
import * as campaignController from './controllers/campaign';
import * as donationController from './controllers/donation';
import * as socialController from './controllers/social';
import * as contactController from './controllers/contact';
import * as privacyController from './controllers/privacy';

const app = express();

// Express configuration
app.set('port', process.env.PORT || 8000);
app.set('views', `${__dirname}/../views`);
app.set('view engine', 'pug');
// causing problems with 304 cached even after file has changed
app.disable('etag');

app.use(compression());
app.use(logger('dev'));
app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ limit: '5mb', extended: true }));
app.use(
  session({
    resave: true,
    saveUninitialized: true,
    secret: process.env.OR_SESSION_SECRET
  })
);
app.use(flash());
app.use(lusca.xframe('SAMEORIGIN'));
app.use(lusca.xssProtection(true));

AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  signatureVersion: 'v4',
  region: process.env.AWS_REGION
});

const s3 = new AWS.S3();

const upload = multer({
  storage: multerS3({
    s3,
    bucket: 'upload.one-relief.org',
    acl: 'public-read',
    contentType: multerS3.AUTO_CONTENT_TYPE,
    metadata: (req: any, file: any, cb: any) =>
      cb(null, { fieldName: file.fieldname }),
    key: (req: any, file: any, cb: any) => cb(null, Date.now().toString())
  })
});

// dist public dir only, because it contains the browserified
// js files, sass processed css, and main.js.
const publicPath = path.join(process.cwd(), 'dist/public/');
app.use(express.static(publicPath, { maxAge: 31557600000 }));

/**
 * Primary app routes.
 */
app.get('/', homeController.index);
app.get('/healthy', homeController.healthy);
app.get('/onboarding', homeController.onboardOne);
app.get('/onboarding2', homeController.onboardTwo);
app.get('/onboarding3', homeController.onboardThree);

app.get('/about', aboutController.index);
app.get('/volunteer', volunteerController.index);
contactController.routes(app);
app.get('/privacy', privacyController.index);

app.get('/campaigns', homeController.index);
app.get('/campaigns/:id', campaignController.index);

app.get('/campaigns/:id/donation', donationController.index);
app.get('/donation/method', donationController.method);
app.get('/donation/amount', donationController.value);
app.post('/campaigns/:id/donation', donationController.checkout);
app.get('/campaigns/:id/donation/success', donationController.success);

app.get('/donations/:id/share', socialController.share);
app.put('/donations/:id/share', socialController.customSelect);
app.post('/social/overlay', socialController.overlay);
app.post('/social/select', socialController.select);
app.post(
  '/donations/:id/share',
  upload.single('share'),
  socialController.userPhotos
);

export { app };
