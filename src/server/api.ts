import fetch, { Response as resp } from 'node-fetch';
import { resolve } from 'url';

const campaignUrl = (...fragments: string[]) =>
  resolve(process.env.OR_API, `campaigns/${fragments.join('/')}`);

const donationUrl = (...fragments: string[]) =>
  resolve(process.env.OR_API, `donations/${fragments.join('/')}`);

const photosUrl = (...fragments: string[]) =>
  resolve(process.env.OR_API, `userphotos/${fragments.join('/')}`);

const api = {
  getCampaignDetails(id: string): Promise<Campaign> {
    return fetch(campaignUrl(id))
      .then((data: resp) => {
        console.log(resp);
        if (data.ok) {
          return data.json();
        }
        throw new Error('bad response code from fetch campaign details');
      })
      .then((json: any) => {
        return json;
      });
  },
  getCampaigns(): Promise<Campaigns> {
    return fetch(campaignUrl())
      .then((data: resp) => {
        if (data.ok) {
          return data.json();
        }
        throw new Error('bad response code fetching campaigns');
      })
      .then((json: { items: Campaigns }) => {
        return json.items;
      })
      .catch((err: resp) => {
        console.error(err);
        throw new Error('unable to fetch campaigns');
      });
  },

  getDonation(id: string): Promise<Donation> {
    return fetch(donationUrl(id))
      .then((data: resp) => {
        if (data.ok) {
          return data.json();
        }
        throw new Error('bad response code fetching donation');
      })
      .then((json: Donation) => {
        return json;
      });
  },

  postImage(data: any): Promise<Photo> {
    return fetch(photosUrl(), {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
      .then((res: resp) => res.json())
      .catch((err: resp) => {
        console.log(err);
        throw new Error('photo was not saved');
      });
  }
};

export { api };
