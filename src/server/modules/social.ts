const staticShares: any = {
  '1': '/images/social/be-a-global-citizen.jpg',
  '2': '/images/social/give-socially.jpg'
};

function emailSubject(campaign: Campaign) {
  return `Join me and help victims of ${campaign.title}`;
}

function emailBody(campaign: Campaign, shareUrl: string) {
  return `Hi there,

  👉 Join me and help victims of ${campaign.title} with a small donation to ${
    campaign.charity.name
  } through OneRelief. 

  Join us at ${shareUrl} and make a difference 😇`;
}

export { staticShares, emailBody, emailSubject };
