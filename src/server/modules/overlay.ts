import * as child_process from 'child_process';
import * as path from 'path';
import { promisify } from 'util';

const IHELPEDROHREGUGEES_OVERLAY: number = 1;
const IAMRELIEFWARRIOR_OVERLAY: number = 2;

const applyOverlay = (imagePath: string, overlayPathId: number) => {
  const childExec = promisify(child_process.execFile);
  const imageDirectory = path.dirname(imagePath);
  const extension = path.extname(imagePath);
  const fileName = path.basename(imagePath, extension);

  const newImageWithOverlayPath = path.join(
    imageDirectory,
    `${fileName}_withoverlay_${Date.now()}${extension}`
  );

  return childExec('identify', ['-format', '%[fx:w]x%[fx:h]', imagePath])
    .then(({ stdout, stderr }) => {
      const overlayPath =
        overlayPathId === IHELPEDROHREGUGEES_OVERLAY
          ? `${__dirname}/../svg/ihelpedrohrefugeees.svg`
          : `${__dirname}/../svg/iamareliefwarrior.svg`;

      return childExec('composite', [
        '-compose',
        'atop',
        '-background',
        'none',
        '-resize',
        stdout,
        overlayPath,
        imagePath,
        newImageWithOverlayPath
      ]);
    })
    .then(({ stdout, stderr }) => {
      return Promise.resolve(newImageWithOverlayPath);
    });
};

export { applyOverlay, IHELPEDROHREGUGEES_OVERLAY, IAMRELIEFWARRIOR_OVERLAY };
