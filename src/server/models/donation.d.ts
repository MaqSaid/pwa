declare interface Donation {
  id?: string;
  amount: number;
  campaign: string;
  donorDetails: {
    email: string;
    name: string;
    address: string;
    country: string;
  };
  madeAt: Date;
}
