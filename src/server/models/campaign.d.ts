declare interface Campaign {
  id: string;
  title: string;
  active: boolean;
  endDate: Date;
  details: {
    images: any;
    summary: string;
    description: string;
    areaAffected: string;
    peopleAffected: string;
    helpRequired: string;
    location: {
      lat: number;
      lng: number;
      zoom: number;
      radius: number;
    };
  };
  fundraising: {
    goal: {
      currency: string;
      value: number;
    };
    donations: number;
    amountRaised: number;
  };
  charity: Charity;
  updatedAt: Date;
  createdAt: Date;
}

type Campaigns = Campaign[];

declare interface Location {
  lat: number;
  lng: number;
  radius: number;
  zoom: number;
}
